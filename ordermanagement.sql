/*
 Navicat Premium Data Transfer

 Source Server         : OrderManagement@localhost
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : ordermanagement

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 22/04/2021 20:39:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client`  (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  UNIQUE INDEX `Client_email_uindex`(`email`) USING BTREE,
  UNIQUE INDEX `Client_id_uindex`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES (2, 'Dragos Brumar', 'dragosbrumar@gmail.com', 'Str. Republicii 4, Cluj-Napoca');
INSERT INTO `client` VALUES (3, 'Iulia Ghinea', 'iuliaghinea@gmail.com', 'Str. Paris 13, Bucuresti');
INSERT INTO `client` VALUES (6, 'Mihai Buciuman', 'mihaibuciuman@gmail.com', 'Str. Nucului 5, Iasi');
INSERT INTO `client` VALUES (4, 'Paul Pop', 'paulpop@gmail.com', 'Str. Unirii 22, Timisoara');
INSERT INTO `client` VALUES (1, 'Serban Pasca', 'serbanpasca@gmail.com', 'Str. Petre Dulfu 1, Baia Mare');
INSERT INTO `client` VALUES (5, 'Teodora Pocol', 'teodorapocol@gmail.com', 'Str. Aurel Vlaicu 11, Brasov');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int NOT NULL,
  `clientId` int NOT NULL,
  `productId` int NOT NULL,
  `date` date NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `Order_id_uindex`(`id`) USING BTREE,
  INDEX `order_client_id_fk`(`clientId`) USING BTREE,
  INDEX `order_product_id_fk`(`productId`) USING BTREE,
  CONSTRAINT `order_client_id_fk` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `order_product_id_fk` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1, 2, 8, '2021-04-06', 3);
INSERT INTO `order` VALUES (2, 4, 1, '2021-04-09', 5);
INSERT INTO `order` VALUES (3, 2, 5, '2021-04-11', 11);
INSERT INTO `order` VALUES (4, 5, 7, '2021-04-14', 6);
INSERT INTO `order` VALUES (5, 5, 3, '2021-04-16', 2);
INSERT INTO `order` VALUES (6, 6, 2, '2021-04-17', 3);
INSERT INTO `order` VALUES (7, 3, 4, '2021-04-17', 1);
INSERT INTO `order` VALUES (8, 1, 1, '2021-04-09', 2);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` double NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `Order_id_uindex`(`id`) USING BTREE,
  UNIQUE INDEX `Order_name_uindex`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, 'Lamp', 25.5, 11);
INSERT INTO `product` VALUES (2, 'Keyboard', 100, 41);
INSERT INTO `product` VALUES (3, 'Toy Truck', 15, 54);
INSERT INTO `product` VALUES (4, 'Headphones', 250, 8);
INSERT INTO `product` VALUES (5, 'Lion King DVD', 4.5, 32);
INSERT INTO `product` VALUES (6, 'Basketball', 75.5, 13);
INSERT INTO `product` VALUES (7, 'Rubber Duck', 2.5, 42);
INSERT INTO `product` VALUES (8, 'Shovel', 35, 14);

SET FOREIGN_KEY_CHECKS = 1;
