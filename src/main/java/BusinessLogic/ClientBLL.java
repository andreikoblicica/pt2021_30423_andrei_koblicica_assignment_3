package BusinessLogic;

import DataAccess.AbstractDAO;
import DataAccess.ClientDAO;
import Model.Client;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;

/**
 * business logic class used for the logic of the application and the connection between the presentation and data access packages
 */
public class ClientBLL {

    private ClientDAO clientDAO=new ClientDAO();

    /**
     * uses the clientDAO to create the clients and returns them
     * @return list of all clients
     */
    public ArrayList<Client> getAllClients(){
        ArrayList<Client> clients=clientDAO.createAllObjects();
        return clients;
    }

    /**
     * uses the clientDAO to create all table columns and returns them
     * @return list of all columns
     */
    public ArrayList<TableColumn<Client, ?>> getTableColumns(){
        ArrayList<TableColumn<Client, ?>> columns= clientDAO.createAllColumns();
        return columns;
    }

    /**
     * checks if the email already exists in the table
     * @param email
     * @param clientId
     * @return
     */
    public boolean validateEmail(String email, int clientId){
       for(Client client: clientDAO.createAllObjects()){
           if(client.getEmail().equals(email) && client.getId()!=clientId){
               return false;
           }
       }
       return true;
    }

    /**
     * uses clientDAO to add the new client to the database
     * @param client new client
     */
    public void addClient(Client client){
        clientDAO.insert(client);
    }
    /**
     * uses clientDAO to edit the new client in the database
     * @param client edited client
     */
    public void editClient(Client client){
        clientDAO.update(client);
    }
    /**
     * uses clientDAO to remove the client from the database
     * @param client removed client
     */
    public void removeClient(Client client) {
        clientDAO.remove(client.getId());
    }
    /**
     * uses clientDAO to get the maximum id from the table
     */
    public int getMaxId(){
        return clientDAO.getMaxId();
    }
}
