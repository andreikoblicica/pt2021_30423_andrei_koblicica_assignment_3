package BusinessLogic;

import DataAccess.AbstractDAO;
import DataAccess.ProductDAO;
import Model.Client;
import Model.Product;
import javafx.scene.control.TableColumn;

import java.util.ArrayList;

/**
 * business logic class used for the logic of the application and the connection between the presentation and data access packages
 */
public class ProductBLL {
    private ProductDAO productDAO=new ProductDAO();

    /**
     * uses the productDAO to create the products and returns them
     * @return list of all products
     */
    public ArrayList<Product> getAllProducts(){
        ArrayList<Product> products=productDAO.createAllObjects();
        return products;
    }
    /**
     * uses the productDAO to create all table columns and returns them
     * @return list of all columns
     */
    public ArrayList<TableColumn<Product, ?>> getTableColumns(){
        ArrayList<TableColumn<Product, ?>> columns= productDAO.createAllColumns();
        return columns;
    }
    /**
     * uses productDAO to add the new product to the database
     * @param product new product
     */
    public void addProduct(Product product){
        productDAO.insert(product);
    }
    /**
     * uses productDAO to edit the product in the database
     * @param product edited product
     */
    public void editProduct(Product product){
        productDAO.update(product);
    }
    /**
     * uses productDAO to remove the product from the database
     * @param product removed product
     */
    public void removeProduct(Product product) {
        productDAO.remove(product.getId());
    }

    /**
     * validates the data inserted by the user
     * @param name name of the product
     * @param price price of the product
     * @param quantity quantity in stock of the product
     * @param id unique identifier of the product
     * @return
     */
    public boolean validateData(String name, double price, int quantity, int id){
        for(Product product: productDAO.createAllObjects()){
            if(product.getName().equals(name) && product.getId()!=id){
                return false;
            }
            if(price<=0 || quantity<=0){
                return false;
            }
        }
        return true;
    }
    /**
     * uses productDAO to get the maximum id from the table
     */
    public int getMaxId(){
        return productDAO.getMaxId();
    }
}
