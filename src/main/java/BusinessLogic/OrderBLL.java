package BusinessLogic;

import DataAccess.AbstractDAO;
import DataAccess.OrderDAO;
import DataAccess.ProductDAO;
import Model.Client;
import Model.Order;
import Model.Product;
import javafx.scene.control.TableColumn;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * business logic class used for the logic of the application and the connection between the presentation and data access packages
 */
public class OrderBLL {

    private OrderDAO orderDAO=new OrderDAO();

    /**
     * uses the orderDAO to create the orders and returns them
     * @return list of all orders
     */
    public ArrayList<Order> getAllOrders(){
        ArrayList<Order> orders=orderDAO.createAllOrders();
        return orders;
    }

    /**
     * uses the orderDAO to create all table columns and returns them
     * @return list of all columns
     */
    public ArrayList<TableColumn<Order, ?>> getTableColumns(){
        ArrayList<TableColumn<Order, ?>> columns= orderDAO.createAllColumns();
        return columns;
    }

    /**
     * uses the orderDAO to insert a new order in the database
     * @param order order to be inserted
     * @param clientId id of the client the order belongs to
     * @param productId id of the product ordered
     */
    public void insertOrder(Order order, int clientId, int productId){
        orderDAO.insertOrder(order,clientId,productId);
    }

    /**
     * checks if the order can be made based on quantity in stock
     * creates the order if possible
     * @param quantity quantity the client wants to order
     * @param product product the client wants to order
     * @return returns the error message to be displayed in case of unsuccessful validation
     */
    public String validateOrder(int quantity, Product product){
        if(quantity<=0){
            return "Make sure you insert correct values!";
        }
        if(quantity> product.getQuantity()){
            return "Insufficient stock!";
        }
        product.setQuantity(product.getQuantity()-quantity);
        ProductDAO productDAO=new ProductDAO();
        productDAO.update(product);
        return "";
    }

    /**
     * uses orderDAO to get the maximum id from the table
     */
    public int getMaxId(){
        return orderDAO.getMaxId();
    }

    /**
     * creates a new file that represents the bill every time a new order is made
     * @param order order that was made
     * @param product product ordered
     */
    public void createBill(Order order, Product product){
        File file=new File("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_3\\"+"order"+order.getId()+".txt");
        try {
            FileWriter fileWriter=new FileWriter(file);
            fileWriter.write("This is a bill for order #"+order.getId()+"\n\n");
            fileWriter.write("Client name: "+order.getClientName()+"\n");
            fileWriter.write("Product name: "+order.getProductName()+"\n");
            fileWriter.write("Order date: "+order.getDate()+"\n");
            fileWriter.write("Quantity: "+order.getQuantity()+"\n");
            fileWriter.write("Total price: "+order.getQuantity()*product.getPrice());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
