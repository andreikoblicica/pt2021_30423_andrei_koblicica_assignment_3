package Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.application.Application;

/**
 * Main class that launches the JavaFx Application
 */
public class Main extends Application{

    /**
     * loads the initial scene onto the primary stage
     * @param primaryStage stage that shows up when the application is launched
     * @throws Exception for invalid .fxml file
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/MainMenu.fxml"));
        Parent root = loader.load();

        primaryStage.setTitle("Order Management System");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
