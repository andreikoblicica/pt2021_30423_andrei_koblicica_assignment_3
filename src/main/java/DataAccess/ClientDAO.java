package DataAccess;

import Model.Client;

/**
 * class used to instantiate the AbstractDAO generic abstract class using the Client type
 */
public class ClientDAO extends AbstractDAO<Client>{
   public ClientDAO() {
       super();
   }
}
