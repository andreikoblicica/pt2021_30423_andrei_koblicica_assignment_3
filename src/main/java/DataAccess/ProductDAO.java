package DataAccess;

import Model.Product;

/**
 * class used to instantiate the AbstractDAO generic abstract class using the Product type
 */
public class ProductDAO extends AbstractDAO<Product>{
    public ProductDAO(){
        super();
    }
}
