package DataAccess;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * generic abstract class used for accessing the database
 * uses reflection techniques
 * @param <T> type of the object
 */
public abstract class AbstractDAO<T> {

    private Class<T> type;

    /**
     * constructor, initializes the type field
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * creates the query necessary for selecting everything in a table based on the current type
     * @return String containing the query
     */
    private String createSelectAllQuery(){
        StringBuilder stringBuilder=new StringBuilder("Select * from ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append(" order by id");
        return stringBuilder.toString();
    }
    /**
     * creates the query necessary for inserting an entry in a table
     * @return String containing the query
     */
    private String createInsertQuery(){
        StringBuilder stringBuilder=new StringBuilder("Insert into ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append(" values (");
        for(Field field: type.getDeclaredFields()){
            stringBuilder.append("?,");
        }
        stringBuilder.setLength(stringBuilder.length()-1);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
    /**
     * creates the query necessary for updating an entry in a table
     * @return String containing the query
     */
    private String createUpdateQuery(){
        StringBuilder stringBuilder=new StringBuilder("Update ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append(" set ");
        for(Field field: type.getDeclaredFields()){
            stringBuilder.append(field.getName());
            stringBuilder.append("=?, ");
        }
        stringBuilder.setLength(stringBuilder.length()-2);
        stringBuilder.append(" where id=?");
        return stringBuilder.toString();
    }
    /**
     * creates the query necessary for removing an entry in a table
     * @return String containing the query
     */
    private String createRemoveQuery(){
        StringBuilder stringBuilder=new StringBuilder("Delete from ");
        stringBuilder.append(type.getSimpleName());
       stringBuilder.append(" where id=?");
        return stringBuilder.toString();
    }
    /**
     * creates the query necessary for obtaining the maximum id a table
     * used for generating the id of a new entry
     * @return String containing the query
     */
    private String createMaxIdQuery(){
        StringBuilder stringBuilder=new StringBuilder("Select max(id) from ");
        if(type.getSimpleName().equals("Order")){
            stringBuilder.append("`");
        }
        stringBuilder.append(type.getSimpleName());
        if(type.getSimpleName().equals("Order")){
            stringBuilder.append("`");
        }
        return stringBuilder.toString();
    }

    /**
     * inserts a given object into the table corresponding to the class type
     * gets the query created for insertion, sets the corresponding values and executes it
     * @param object the object to be inserted into the database
     */
    public void insert(Object object){
        DatabaseConnection databaseConnection=new DatabaseConnection();
        try{
            int i=1;
            Object value;
            PreparedStatement statement=databaseConnection.getConnection().prepareStatement(createInsertQuery());
            for(Field field: type.getDeclaredFields()){
                field.setAccessible(true);
                value=field.get(object);
                statement.setObject(i,value);
                i++;
            }
            statement.executeUpdate();
            databaseConnection.close(statement);
        }catch (SQLException exception){
            exception.printStackTrace();
        }catch (IllegalAccessException exception){
            exception.printStackTrace();
        }
        databaseConnection.closeConnection();
    }
    /**
     * edits the entry corresponding to the given object in the database table
     * gets the query created for edit, sets the corresponding values and executes it
     * @param object the object to be edited in the database
     */
    public void update(Object object){
        DatabaseConnection databaseConnection=new DatabaseConnection();
        try{
            int i=1;
            Object value;
            PreparedStatement statement=databaseConnection.getConnection().prepareStatement(createUpdateQuery());
            for(Field field: type.getDeclaredFields()){
                field.setAccessible(true);
                value=field.get(object);
                statement.setObject(i,value);
                i++;
            }
            for(Field field: type.getDeclaredFields()){
                field.setAccessible(true);
                if(field.getName().equals("id")){
                    value=field.get(object);
                    statement.setObject(i,value);
                }
            }
            statement.executeUpdate();
            databaseConnection.close(statement);
        }catch (SQLException exception){
            exception.printStackTrace();
        }catch (IllegalAccessException exception){
            exception.printStackTrace();
        }
        databaseConnection.closeConnection();
    }

    /**
     * removes a given object from the database table corresponding to the class type
     * gets the query created for removal, sets the corresponding values and executes it
     * @param id the id of the object to be removed from the table in the database
     */
    public void remove(int id){
        DatabaseConnection databaseConnection=new DatabaseConnection();
        try{
            PreparedStatement statement=databaseConnection.getConnection().prepareStatement(createRemoveQuery());
            statement.setInt(1,id);
            statement.executeUpdate();
            databaseConnection.close(statement);
        }catch (SQLException exception){
            exception.printStackTrace();
        }
        databaseConnection.closeConnection();
    }

    /**
     * gets the maximum id in a table by executing the generated query
     * @return value of the maximum id
     */
    public int getMaxId(){
        DatabaseConnection databaseConnection=new DatabaseConnection();
        try{
            Statement statement=databaseConnection.getStatement();
            ResultSet resultSet=statement.executeQuery(createMaxIdQuery());
            if(resultSet.next()){
                return resultSet.getInt(1);
            }
            else return 1;
        }catch (SQLException exception){
            exception.printStackTrace();
        }
        return -1;
    }

    /**
     * creates objects of class type based on all entries inside a table
     * executes the select all query and creates objects while the result set of the query is not yet empty
     * @return list of objects created
     */
    public ArrayList<T> createAllObjects(){
        ArrayList<T> arrayList=new ArrayList<>();
        DatabaseConnection databaseConnection=new DatabaseConnection();
        Statement statement= databaseConnection.getStatement();
        try {
            ResultSet resultSet=statement.executeQuery(createSelectAllQuery());
            while(resultSet.next()){
                T instance=type.newInstance();
                for(Field field: type.getDeclaredFields()){
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                arrayList.add(instance);
            }
            databaseConnection.close(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        databaseConnection.close(statement);
        databaseConnection.closeConnection();
        return arrayList;
    }

    /**
     * generates trough reflection the header of the table corresponding to the class
     * calls the createTableColumn method for each field
     * @return list of table columns necessary to generate a table view
     */
    public ArrayList<TableColumn<T, ?>> createAllColumns(){
        ArrayList<TableColumn<T, ?>> columns=new ArrayList<>();
        for(Field field: type.getDeclaredFields()){
            TableColumn<T, ?> column = createTableColumn(field);
           columns.add(column);
        }
        return columns;
    }

    /**
     * uses reflection to set the type of the column that is created
     * @param field field corresponding to the class the table represents
     * @param <S> the type of the field
     * @return new column to be added to the column list
     */
    public <S> TableColumn<T,S> createTableColumn(Field field) {
        TableColumn<T, S> column =  new TableColumn<T,S>(field.getName());
        column.setCellValueFactory(cellData -> {
            try {
                boolean wasAccessible = field.isAccessible() ;
                field.setAccessible(true);
                @SuppressWarnings("unchecked")
                SimpleObjectProperty<S> property = new SimpleObjectProperty<S>((S)(field.get(cellData.getValue())));
                field.setAccessible(wasAccessible);
                return property;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        return column;
    }
}
