package DataAccess;

import Model.Order;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;

/**
 * class used to instantiate the AbstractDAO abstract class using the Order type
 */
public class OrderDAO extends AbstractDAO<Order>{

    private final static String getAllOrdersQuery="select order.id,Client.name,Product.name,date,order.quantity from `order` join Client on clientId=Client.id join Product on productId=Product.id";
    private final static String insertOrderQuery="insert into `order` values (?,?,?,?,?)";

    /**
     * creates orders corresponding to all entries in the Order table
     * executes the getAllOrdersQuery that joins the Client and Product tables
     * @return list of all orders in the database
     */
    public ArrayList<Order> createAllOrders(){
        ArrayList<Order> orders=new ArrayList<>();
        DatabaseConnection databaseConnection=new DatabaseConnection();
        Statement statement= databaseConnection.getStatement();
        try {
            ResultSet resultSet = statement.executeQuery(getAllOrdersQuery);
            while(resultSet.next()) {
                int id=resultSet.getInt(1);
                String clientName=resultSet.getString(2);
                String productName=resultSet.getString(3);
                Date date=resultSet.getDate(4);
                int quantity=resultSet.getInt(5);
                Order order=new Order(id,clientName,productName,date,quantity);
                orders.add(order);
            }
            databaseConnection.close(resultSet);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        databaseConnection.close(statement);
        databaseConnection.closeConnection();
        return orders;
    }

    /**
     * inserts a new order in the Orders table from the database
     * needs clientId and productId since Order is a join table
     * @param order the order to be inserted
     * @param clientId the id of the client corresponding to the order
     * @param productId the id of the product corresponding to the order
     */
    public void insertOrder(Order order, int clientId, int productId){
        DatabaseConnection databaseConnection=new DatabaseConnection();
        try{
            PreparedStatement statement=databaseConnection.getConnection().prepareStatement(insertOrderQuery);
            statement.setInt(1,order.getId());
            statement.setInt(2,clientId);
            statement.setInt(3,productId);
            statement.setDate(4,order.getDate());
            statement.setInt(5,order.getQuantity());
            statement.executeUpdate();
            databaseConnection.close(statement);
        }catch (SQLException exception){
            exception.printStackTrace();
        }
        databaseConnection.closeConnection();
    }
}
