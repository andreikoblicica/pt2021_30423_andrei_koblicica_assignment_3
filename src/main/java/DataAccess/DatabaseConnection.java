package DataAccess;

import java.sql.*;

/**
 * class used to create the connection with the database
 */
public class DatabaseConnection {

    private Connection connection;
    private Statement statement;
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/OrderManagement";
    private static final String USER = "root";
    private static final String PASSWORD = "kindsoflight";

    /**
     * constructor, creates the connection using the necessary Strings, as well as a new statement
     */
    public DatabaseConnection() {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * closes the database connection
     */
    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * closes a statement that was created somewhere else and received as parameter
     * @param statement statement to close
     */
    public void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException exception) {
               exception.printStackTrace();
            }
        }
    }

    /**
     * closes a prepared statement that was created somewhere else and received as parameter
     * @param preparedStatement prepared statement to close
     */
    public void close(PreparedStatement preparedStatement) {
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * closes a result set that was created somewhere else and received as parameter
     * @param resultSet result set to close
     */
    public void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException exception) {
              exception.printStackTrace();
            }
        }
    }

    /**
     * returns the connection that was created
     * @return connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * returns the statement that was created
     * @return statement
     */
    public Statement getStatement() {
        return statement;
    }
}
