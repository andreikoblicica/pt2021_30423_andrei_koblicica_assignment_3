package Model;

import java.sql.Date;

/**
 * class used to model an order with it corresponding attributes
 */
public class Order {
    int id;
    String clientName;
    String productName;
    Date date;
    int quantity;

    /**
     * empty constructor
     */
    public Order(){

    }

    /**
     * constructor, creates a new order with the given parameters
     * @param id unique order identifier
     * @param clientName name of the client the order belongs to
     * @param productName name of the product the client ordered
     * @param date date of the order
     * @param quantity quantity of products the client ordered
     */
    public Order(int id, String clientName, String productName, Date date, int quantity) {
        this.id = id;
        this.clientName = clientName;
        this.productName = productName;
        this.date = date;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
