package Model;

/**
 * class used to model a product with its corresponding attributes
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private int quantity;

    /**
     * empty constructor
     */
    public Product(){

    }

    /**
     * constructor, creates a new product with the given parameters
     * @param id unique product identifier
     * @param name name of the product
     * @param price price of the product
     * @param quantity quantity in stock of the product
     */
    public Product(int id, String name, double price, int quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String toString(){
        return name;
    }
}
