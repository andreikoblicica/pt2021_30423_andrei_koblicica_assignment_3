package Presentation;

import DataAccess.AbstractDAO;
import DataAccess.ClientDAO;
import Model.Client;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * controller class for the Main Menu window in the Application
 */
public class MainMenuController {

    @FXML
    private Button clientsButton;
    @FXML
    private Button productsButton;
    @FXML
    private Button ordersButton;

    /**
     * loads the Clients scene when the Clients button is pressed
     * @param event the mouse click causing the method call
     * @throws IOException exception thrown in case the .fxml file is not found
     */
    @FXML
    private void pressClients(ActionEvent event)throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/Clients.fxml"));
        Parent root = loader.load();

        ClientsController clientsController=loader.getController();
        clientsController.initializeScene();

        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * loads the Products scene when the Products button is pressed
     * @param event the mouse click causing the method call
     * @throws IOException exception thrown in case the .fxml file is not found
     */
    @FXML
    private void pressProducts(ActionEvent event)throws IOException{
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/Products.fxml"));
        Parent root = loader.load();

        ProductsController productsController=loader.getController();
        productsController.initializeScene();

        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * loads the Orders scene when the Orders button is pressed
     * @param event the mouse click causing the method call
     * @throws IOException exception thrown in case the .fxml file is not found
     */
    @FXML
    private void pressOrders(ActionEvent event)throws IOException{
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/Orders.fxml"));
        Parent root = loader.load();

        OrdersController ordersController=loader.getController();
        ordersController.initializeScene();

        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

}
