package Presentation;

import BusinessLogic.ProductBLL;
import Model.Client;
import Model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * controller class for the Products window in the Application
 */
public class ProductsController {

    @FXML
    private Button backToMainMenu;

    @FXML
    private Button addProduct;
    @FXML
    private Button editProduct;
    @FXML
    private Button removeProduct;
    @FXML
    private Button submit;
    @FXML
    private Button cancel;

    @FXML
    private Label actionPerformedLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Label quantityLabel;


    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField quantityField;


    @FXML
    private TableView<Product> productsTable;

    private ProductBLL productBLL=new ProductBLL();

    private Product currentProduct=new Product();

    /**
     * creates the table and initializes it with the values from the database
     */
    public void initializeScene(){
        loadTable();
        setVisibility(false);
    }

    /**
     * makes certain fields and labels visible or invisible based on what the user is doing
     * @param visibility boolean that decides the state of fields and labels
     */
    private void setVisibility(boolean visibility){
        actionPerformedLabel.setVisible(visibility);
        nameLabel.setVisible(visibility);
        priceLabel.setVisible(visibility);
        quantityLabel.setVisible(visibility);
        nameField.setVisible(visibility);
        priceField.setVisible(visibility);
        quantityField.setVisible(visibility);
        submit.setVisible(visibility);
        cancel.setVisible(visibility);
    }
    /**
     * creates the table using the columns generated trough reflection and fills it with the orders from the database
     */
    private void loadTable(){
        ArrayList<TableColumn<Product,?>> columns=productBLL.getTableColumns();
        for(TableColumn<Product,?> column: columns){
            productsTable.getColumns().add(column);
        }
        productsTable.setItems(getAllProductsObservableList());
    }

    /**
     * transforms the ArrayList into an ObservableList
     * @return observableList of products
     */
    private ObservableList<Product> getAllProductsObservableList(){
        ObservableList<Product> products= FXCollections.observableArrayList();
        for(Product product: productBLL.getAllProducts()) {
            products.add(product);
        }
        return products;
    }

    /**
     * loads the Main Menu scene when the Back button is pressed
     * @param event the mouse click causing the method call
     * @throws IOException exception thrown in case the .fxml file is not found
     */
    @FXML
    private void pressBackToMainMenu(ActionEvent event)throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/MainMenu.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * displays the necessary things for product addition
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressAddProduct(ActionEvent event){
        setVisibility(true);
        actionPerformedLabel.setText("Add Product:");
        nameField.clear();
        priceField.clear();
        quantityField.clear();
    }
    /**
     * displays the necessary things for product editing
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressEditProduct(ActionEvent event){
        if(productsTable.getSelectionModel().getSelectedItem()!=null) {
            setVisibility(true);
            actionPerformedLabel.setText("Edit Product:");
            if (productsTable.getSelectionModel().getSelectedItem() != null) {
                currentProduct = productsTable.getSelectionModel().getSelectedItem();
                nameField.setText(currentProduct.getName());
                priceField.setText("" + currentProduct.getPrice());
                quantityField.setText("" + currentProduct.getQuantity());
            }
        }
    }
    /**
     * removes the currently selected client from the table
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressRemoveProduct(ActionEvent event){
        if(productsTable.getSelectionModel().getSelectedItem()!=null) {
            Product product = productsTable.getSelectionModel().getSelectedItem();
            productBLL.removeProduct(product);
            productsTable.setItems(getAllProductsObservableList());
        }
    }
    /**
     * collects the data from the text fields completed by the user and performs the add or edit product operation
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressSubmit(ActionEvent event){
        int id;
        if(nameField.getText().equals("")||priceField.getText().equals("")||quantityField.getText().equals("")){
            loadAlertBox("Make sure you complete all fields!");
            return;
        }
        if(actionPerformedLabel.getText().equals("Add Product:")){
            id=-1;
        }else{
            id=currentProduct.getId();
        }
        try {
            double price = Double.parseDouble(priceField.getText());
            int quantity = Integer.parseInt(quantityField.getText());
            if(!productBLL.validateData(nameField.getText(),price,quantity,id)){
                loadAlertBox("Make sure you insert correct values!");
                return;
            }
            if(actionPerformedLabel.getText().equals("Add Product:")){
                Product product=new Product(productBLL.getMaxId()+1, nameField.getText(),price,quantity);
                productBLL.addProduct(product);
            }
            else{
                currentProduct.setName(nameField.getText());
                currentProduct.setPrice(price);
                currentProduct.setQuantity(quantity);
                productBLL.editProduct(currentProduct);
            }
            productsTable.setItems(getAllProductsObservableList());
            setVisibility(false);
        }catch (NumberFormatException exception){
           loadAlertBox("Make sure you insert correct values!");
        }

    }

    /**
     * sets visibility back to invisible
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressCancel(ActionEvent event){
        setVisibility(false);
    }

    /**
     * loads a pop-up error window with a given error message
     * @param message message to be displayed
     */
    private void loadAlertBox(String message){
        Stage stage=new Stage();
        stage.setTitle("Error");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader=new FXMLLoader(getClass().getResource("/AlertBox.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
