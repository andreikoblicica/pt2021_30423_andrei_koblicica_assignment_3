package Presentation;

import BusinessLogic.ClientBLL;
import DataAccess.AbstractDAO;
import DataAccess.ClientDAO;
import DataAccess.DatabaseConnection;
import Model.Client;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

/**
 * controller class for the Clients window in the Application
 */
public class ClientsController {

    @FXML
    private Button backToMainMenu;

    @FXML
    private Button addClient;
    @FXML
    private Button editClient;
    @FXML
    private Button removeClient;
    @FXML
    private Button submit;
    @FXML
    private Button cancel;

    @FXML
    private Label actionPerformedLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label emailLabel;
    @FXML
    private Label addressLabel;


    @FXML
    private TextField nameField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField addressField;

    @FXML
    private TableView<Client> clientsTable;

    private ClientBLL clientBLL=new ClientBLL();

    private Client currentClient=new Client();

    /**
     * creates the table and initializes it with the values from the database
     */
    public void initializeScene(){
       loadTable();
       setVisibility(false);
    }

    /**
     * makes certain fields and labels visible or invisible based on what the user is doing
     * @param visibility boolean that decides the state of fields and labels
     */
    private void setVisibility(boolean visibility){
        actionPerformedLabel.setVisible(visibility);
        nameLabel.setVisible(visibility);
        emailLabel.setVisible(visibility);
        addressLabel.setVisible(visibility);
        nameField.setVisible(visibility);
        emailField.setVisible(visibility);
        addressField.setVisible(visibility);
        submit.setVisible(visibility);
        cancel.setVisible(visibility);
    }

    /**
     * creates the table using the columns generated trough reflection and fills it with the clients from the database
     */
    private void loadTable(){
        ArrayList<TableColumn<Client,?>> columns=clientBLL.getTableColumns();
        for(TableColumn<Client,?> column: columns){
            clientsTable.getColumns().add(column);
        }
        clientsTable.setItems(getAllClientsObservableList());
    }

    /**
     * transforms the ArrayList into an ObservableList
     * @return observableList of clients
     */
    private ObservableList<Client> getAllClientsObservableList(){
        ObservableList<Client> clients= FXCollections.observableArrayList();
        for(Client client: clientBLL.getAllClients()) {
            clients.add(client);
        }

        return clients;
    }

    /**
     * loads the Main Menu scene when the Back button is pressed
     * @param event the mouse click causing the method call
     * @throws IOException exception thrown in case the .fxml file is not found
     */
    @FXML
    private void pressBackToMainMenu(ActionEvent event)throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/MainMenu.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * displays the necessary things for client addition
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressAddClient(ActionEvent event){
        setVisibility(true);
        actionPerformedLabel.setText("Add Client:");
        nameField.clear();
        emailField.clear();
        addressField.clear();
    }
    /**
     * displays the necessary things for client editing
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressEditClient(ActionEvent event){
        if(clientsTable.getSelectionModel().getSelectedItem()!=null) {
            setVisibility(true);
            actionPerformedLabel.setText("Edit Client:");
            if (clientsTable.getSelectionModel().getSelectedItem() != null) {
                currentClient = clientsTable.getSelectionModel().getSelectedItem();
                nameField.setText(currentClient.getName());
                emailField.setText(currentClient.getEmail());
                addressField.setText(currentClient.getAddress());
            }
        }
    }
    /**
     * removes the currently selected client from the table
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressRemoveClient(ActionEvent event){
        if(clientsTable.getSelectionModel().getSelectedItem()!=null) {
            Client client = clientsTable.getSelectionModel().getSelectedItem();
            clientBLL.removeClient(client);
            clientsTable.setItems(getAllClientsObservableList());
        }
    }

    /**
     * collects the data from the text fields completed by the user and performs the add or edit client operation
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressSubmit(ActionEvent event){
        int id;
        if(nameField.getText().equals("")||emailField.getText().equals("")||addressField.getText().equals("")){
            loadAlertBox("make sure you complete all fields!");
            return;
        }
        if(actionPerformedLabel.getText().equals("Add Client:")){
            id=-1;
        }else{
            id=currentClient.getId();
        }
        if(!clientBLL.validateEmail(emailField.getText(),id)){
            loadAlertBox("Email already exists!");
            return;
        }
        if(actionPerformedLabel.getText().equals("Add Client:")){
            Client client=new Client(clientBLL.getMaxId()+1,nameField.getText(),emailField.getText(),addressField.getText());
            clientBLL.addClient(client);

        }
        else{
            currentClient.setName(nameField.getText());
            currentClient.setEmail(emailField.getText());
            currentClient.setAddress(addressField.getText());
            clientBLL.editClient(currentClient);
        }
        clientsTable.setItems(getAllClientsObservableList());
        setVisibility(false);
    }

    /**
     * sets visibility back to invisible
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressCancel(ActionEvent event){
        setVisibility(false);
    }

    /**
     * loads a pop-up error window with a given error message
     * @param message message to be displayed
     */
    private void loadAlertBox(String message){
        Stage stage=new Stage();
        stage.setTitle("Error");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader=new FXMLLoader(getClass().getResource("/AlertBox.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }

}
