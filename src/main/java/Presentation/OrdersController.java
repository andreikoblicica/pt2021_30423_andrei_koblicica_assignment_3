package Presentation;

import BusinessLogic.ClientBLL;
import BusinessLogic.OrderBLL;
import BusinessLogic.ProductBLL;
import DataAccess.AbstractDAO;
import DataAccess.OrderDAO;
import Model.Client;
import Model.Order;
import Model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

/**
 * controller class for the Orders window in the Application
 */
public class OrdersController {

    @FXML
    private Button backToMainMenu;
    @FXML
    private Button submit;
    @FXML
    private Button clear;

    @FXML
    private ChoiceBox<Client> clientChoiceBox;
    @FXML
    private ChoiceBox<Product> productChoiceBox;

    @FXML
    private TextField quantityField;

    @FXML
    private DatePicker datePicker;

    @FXML
    private TableView<Order> ordersTable;

    OrderBLL orderBLL=new OrderBLL();
    ClientBLL clientBLL=new ClientBLL();
    ProductBLL productBLL=new ProductBLL();

    /**
     * creates the table and initializes it with the values from the database
     */
    public void initializeScene(){
       loadTable();
       initializeChoiceBoxes();
    }

    /**
     * adds all the clients and products to the drop down lists
     */
    private void initializeChoiceBoxes() {
        clientChoiceBox.getItems().addAll(clientBLL.getAllClients());
        productChoiceBox.getItems().addAll(productBLL.getAllProducts());
    }

    /**
     * creates the table using the columns generated trough reflection and fills it with the orders from the database
     */
    private void loadTable(){
        ArrayList<TableColumn<Order,?>> columns=orderBLL.getTableColumns();
        for(TableColumn<Order,?> column: columns){
            ordersTable.getColumns().add(column);
        }
        ordersTable.setItems(getAllOrdersObservableList());
    }

    /**
     * transforms the ArrayList into an ObservableList
     * @return observableList of orders
     */
    private ObservableList<Order> getAllOrdersObservableList(){
        ObservableList<Order> orders= FXCollections.observableArrayList();
        for(Order order: orderBLL.getAllOrders()) {
            orders.add(order);
        }
        return orders;
    }

    /**
     * clears all fields
     */
    private void clear(){
        datePicker.getEditor().clear();
        datePicker.setValue(null);
        quantityField.clear();
        clientChoiceBox.getSelectionModel().clearSelection();
        productChoiceBox.getSelectionModel().clearSelection();
    }

    /**
     * loads the Main Menu scene when the Back button is pressed
     * @param event the mouse click causing the method call
     * @throws IOException exception thrown in case the .fxml file is not found
     */
    @FXML
    private void pressBackToMainMenu(ActionEvent event)throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/MainMenu.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * collects the data from the fields completed by the user and creates the new order
     * @param event the mouse click causing the method call
     */
    @FXML
    private void pressSubmit(ActionEvent event){
        if(clientChoiceBox.getSelectionModel().getSelectedItem()==null || productChoiceBox.getSelectionModel().getSelectedItem()==null || quantityField.getText().equals("") || datePicker.getValue()==null){
            loadAlertBox("Make sure you complete all fields!");
            return;
        }
        int quantity;
        try{
            quantity=Integer.parseInt(quantityField.getText());
        }catch(NumberFormatException exception){
            loadAlertBox("Make sure you insert correct values!");
            return;
        }
        String message=orderBLL.validateOrder(quantity,productChoiceBox.getSelectionModel().getSelectedItem());
        if(!message.equals("")){
            loadAlertBox(message);
            return;
        }
        Order order=new Order(orderBLL.getMaxId()+1, clientChoiceBox.getSelectionModel().getSelectedItem().getName(),productChoiceBox.getSelectionModel().getSelectedItem().getName(), Date.valueOf(datePicker.getValue()),quantity);
        orderBLL.insertOrder(order,clientChoiceBox.getSelectionModel().getSelectedItem().getId(),productChoiceBox.getSelectionModel().getSelectedItem().getId());
        orderBLL.createBill(order,productChoiceBox.getSelectionModel().getSelectedItem());
        ordersTable.setItems(getAllOrdersObservableList());
        clear();
    }

    @FXML
    /**
     * calls the clear method
     * @param event  the mouse click causing the method call
     */
    private void pressClear(ActionEvent event){
        clear();
    }


    /**
     * loads a pop-up error window with a given error message
     * @param message message to be displayed
     */
    private void loadAlertBox(String message){
        Stage stage=new Stage();
        stage.setTitle("Error");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader=new FXMLLoader(getClass().getResource("/AlertBox.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
